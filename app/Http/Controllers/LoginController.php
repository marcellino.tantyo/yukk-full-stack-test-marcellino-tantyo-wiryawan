<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class LoginController extends Controller
{
    function showLoginForm() {
        return view('login');
    }

    function login(Request $request) {

        $registeredUsers = Cache::get('users');

        if (!$registeredUsers) {
            return response()->json('Unauthorized user', 401);
        }

        $isAuthorized = false;
        foreach ($registeredUsers as $user) {
            if ($request->email == $user['email']) {
                $isAuthorized = true;

                Cache::put('userLogin', [[
                    'id' => $user['id']
                ]]);

                break;
            }
        }

        if (!$isAuthorized) {
            return response()->json('Unauthorized user', 401);
        }

        return redirect()->route('index');
    }
}
