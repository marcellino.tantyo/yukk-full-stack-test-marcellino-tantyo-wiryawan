<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class TransactionController extends Controller
{
    public function index()
    {
        return view('index');
    }

    public function store(Request $request)
    {
        // Validate the request data based on your requirements
        $request->validate([
            'type' => 'required|in:topup,transaction',
            'amount' => 'required|numeric',
            'information' => 'required|string',
            'proof' => $request->type === 'topup' ? 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048' : '',
        ]);

        $proofFile = $request->file('proof');

        $originalFileName = $proofFile->getClientOriginalName();

        $proofFile->move('uploads', $originalFileName);

        $userLogin = Cache::get('userLogin');

        if (!$userLogin) {
            return response()->json('Unauthorized user', 401);
        }

        $transactionCode = 'TX' . strtoupper(uniqid());

        $transaction = Cache::get('transaction');
        if ($transaction) {
            $newTransaction = [
                'id' => $userLogin[0]['id'],
                'transactionCode' => $transactionCode,
                'type' => $request->type,
                'amount' => $request->amount,
                'information' => $request->information,
                'proof' => $originalFileName
            ];

            Cache::put('transaction', array_merge(Cache::get('transaction'), [$newTransaction]));
        } else {
            Cache::put('transaction', [[
                'id' => $userLogin[0]['id'],
                'transactionCode' => $transactionCode,
                'type' => $request->type,
                'amount' => $request->amount,
                'information' => $request->information,
                'proof' => $originalFileName
            ]]);
        }

        $registeredUsers = Cache::get('users');

        foreach ($registeredUsers as $user) {
            if ($userLogin[0]['id'] == $user['id']) {
                if ($request->type == "topup") {
                    $balance = $user['balance'] + $request->amount;
                } else {
                    $balance = $user['balance'] - $request->amount;
                }

                Cache::put('users', [[
                    'id' => $userLogin[0]['id'],
                    'username' => $user['username'],
                    'email' => $user['email'],
                    'password' => $user['password'],
                    'token' => $user['token'],
                    'balance' => $balance
                ]]);

                break;
            }
        }
        return redirect()->route('history');
    }
}
