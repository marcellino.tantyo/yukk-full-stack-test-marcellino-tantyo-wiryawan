<?php

namespace App\Http\Controllers;

use DateTime;
use DateTimeZone;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    function showRegistrationForm() {

        return view('register');
    }

    function register(Request $request) {

//        Cache::forget('users');
        $username = $request->username;
        if (!$username || !is_string($username)) {
            return response()->json('Bad Request', 400);
        }

        $registeredUsers = Cache::get('users');
        $token = hash('sha256', Str::random(40));
        if ($registeredUsers) {
            foreach ($registeredUsers as $user) {
                if ($username == $user['username']) {
                    return response()->json('Username already exists', 409);
                }
            }

            $newUser = [
                'id' => count($registeredUsers) + 1,
                'username' => $username,
                'email' => $request->email,
                'password' => $request->password,
                'token' => $token,
                'balance' => 0
            ];

            Cache::put('users', array_merge($registeredUsers, [$newUser]));

            $returnData = [
                'username' => $newUser['username'],
                'email' => $newUser['email'],
                'password' => $newUser['password'],
                'token' => $newUser['token'],
                'balance' => $newUser['balance']
            ];

            return response()->json($returnData, 201);
        }

        Cache::put('users', [[
            'id' => 1,
            'username' => $username,
            'email' => $request->email,
            'password' => $request->password,
            'token' => $token,
            'balance' => 0
        ]]);

        $firstUser = Cache::get('users');

        $returnData = [
            'username' => $firstUser[0]['username'],
            'email' => $firstUser[0]['email'],
            'password' => $firstUser[0]['password'],
            'token' => $firstUser[0]['token'],
            'balance' => $firstUser[0]['balance']
        ];

        return response()->json($returnData, 201);
    }
}
