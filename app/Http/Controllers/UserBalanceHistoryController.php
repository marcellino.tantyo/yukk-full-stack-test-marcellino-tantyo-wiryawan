<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;

class UserBalanceHistoryController extends Controller
{
    public function index(Request $request)
    {
        $history = Cache::get('transaction');
        $userLogin = Cache::get('userLogin');

        $transactions = [];

        foreach ($history as $data) {
            if ($userLogin[0]['id'] == $data['id']) {
                $transactions[] = $data;
            }
        }

        // Paginate the results
//        $transactions = $transactions->paginate(10);

        $registeredUsers = Cache::get('users');
        $balance = 0;

        foreach ($registeredUsers as $user) {
            if ($userLogin[0]['id'] == $user['id']) {
                $balance = $user['balance'];
                break;
            }
        }

        return view('history', compact('transactions', 'balance'));
    }
}
